# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-sphinx-autoapi
_pyname=sphinx_autoapi
pkgver=3.3.0
pkgrel=0
pkgdesc="Automatic API documentation for Sphinx"
url="https://sphinx-autoapi.readthedocs.io"
arch="noarch !x86" # build breaks on x86
license="MIT"
depends="python3 py3-astroid py3-jinja2 py3-sphinx py3-anyascii py3-yaml"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-mock py3-pytest py3-beautifulsoup4"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/s/$_pyname/$_pyname-$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	# Disable tests that require network access
	.testenv/bin/python3 -m pytest \
		--deselect tests/python/test_pyintegration.py::TestPipeUnionModule::test_integration \
		--deselect tests/python/test_pyintegration.py::TestPEP695::test_integration
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}
sha512sums="
b4d815e01f65ed6c278ceeabb1fca4e3d4093e92320744919b339cd10445d65c21e96fa8f03e27d1c983f7e4d0717f2615b0a9d2a71d81aaea3782856ea1a11e  sphinx_autoapi-3.3.0.tar.gz
"
