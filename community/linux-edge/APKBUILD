# Maintainer: Milan P. Stanić <mps@arvanta.net>

_flavor=edge
pkgname=linux-${_flavor}
# NOTE: this kernel is intended for testing
# please resist urge to upgrade it blindly
pkgver=6.10.7
case $pkgver in
	*.*.*)	_kernver=${pkgver%.*};;
	*.*) _kernver=$pkgver;;
esac
pkgrel=0
pkgdesc="Linux latest stable kernel"
url="https://www.kernel.org"
depends="initramfs-generator"
_depends_dev="perl gmp-dev elfutils-dev flex bison"
makedepends="$_depends_dev
	bc
	diffutils
	findutils
	installkernel
	linux-headers
	linux-firmware-any
	openssl-dev
	python3
	sed
	xz"

options="!strip !check" # no tests
_config=${config:-config-edge.${CARCH}}

subpackages="$pkgname-dev:_dev:$CBUILD_ARCH $pkgname-doc:_doc"
source="https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/linux-$_kernver.tar.xz"
case $pkgver in
	*.*.0)	source="$source";;
	*.*.*)	source="$source
	https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/patch-$pkgver.xz" ;;
esac

source="$source
	config-edge.aarch64
	config-edge.armv7
	config-edge.x86_64
	config-edge.riscv64
	"

builddir="$srcdir/linux-${_kernver}"
arch="armv7 aarch64 x86_64 riscv64"
license="GPL-2.0"

_flavors=
for _i in $source; do
	case $_i in
	config-*.$CARCH)
		_f=${_i%.$CARCH}
		_f=${_f#config-}
		_flavors="$_flavors ${_f}"
		if [ "linux-$_f" != "$pkgname" ]; then
			subpackages="$subpackages linux-${_f}::$CBUILD_ARCH linux-${_f}-dev:_dev:$CBUILD_ARCH"
		fi
		;;
	esac
done

_carch=${CARCH}
case "$_carch" in
aarch64*) _carch="arm64" ;;
arm*) _carch="arm" ;;
riscv64) _carch="riscv" ;;
esac

prepare() {
	local _patch_failed=
	cd $builddir
	case $pkgver in
		*.*.0);;
		*)
		msg "Applying patch-$pkgver.xz"
		unxz -c < "$srcdir"/patch-$pkgver.xz | patch -p1 -N ;;
	esac

	# first apply patches in specified order
	for i in $source; do
		case $i in
		*.patch)
			msg "Applying $i..."
			if ! patch -s -p1 -N -i "$srcdir"/$i; then
				echo $i >>failed
				_patch_failed=1
			fi
			;;
		esac
	done

	if ! [ -z "$_patch_failed" ]; then
		error "The following patches failed:"
		cat failed
		return 1
	fi

	# remove localversion from patch if any
	rm -f localversion*
	oldconfig
}

oldconfig() {
	for i in $_flavors; do
		local _config=config-$i.${CARCH}
		mkdir -p "$builddir"
		echo "-$pkgrel-$i" > "$builddir"/localversion-alpine \
			|| return 1

		cp "$srcdir"/$_config "$builddir"/.config
		make -C $builddir \
			O="$builddir" \
			ARCH="$_carch" \
			listnewconfig oldconfig
	done
}

build() {
	unset LDFLAGS
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"
	for i in $_flavors; do
		cd "$builddir"
		make ARCH="$_carch" DTC_FLAGS="-@" CC="${CC:-gcc}" \
			KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-Alpine"
	done
}

_package() {
	local _buildflavor="$1" _outdir="$2"
	local _abi_release=${pkgver}-${pkgrel}-${_buildflavor}
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	cd "$builddir"
	# modules_install seems to regenerate a defect Modules.symvers on s390x. Work
	# around it by backing it up and restore it after modules_install
	cp Module.symvers Module.symvers.backup

	mkdir -p "$_outdir"/boot "$_outdir"/lib/modules

	local _install
	case "$CARCH" in
		arm*|aarch64) _install="zinstall dtbs_install";;
		riscv64) _install="install dtbs_install";;
		*) _install=install;;
	esac

	make -j1 modules_install $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$_outdir" \
		INSTALL_PATH="$_outdir"/boot \
		INSTALL_DTBS_PATH="$_outdir/boot/dtbs-$_buildflavor"

	cp Module.symvers.backup Module.symvers

	rm -f "$_outdir"/lib/modules/${_abi_release}/build \
		"$_outdir"/lib/modules/${_abi_release}/source
	rm -rf "$_outdir"/lib/firmware

	install -D -m644 include/config/kernel.release \
		"$_outdir"/usr/share/kernel/$_buildflavor/kernel.release
}

# main flavor installs in $pkgdir
package() {
	depends="$depends linux-firmware-any"

	_package edge "$pkgdir"
}

_dev() {
	local _flavor=$(echo $subpkgname | sed -E 's/(^linux-|-dev$)//g')
	local _abi_release=${pkgver}-${pkgrel}-$_flavor
	# copy the only the parts that we really need for build 3rd party
	# kernel modules and install those as /usr/src/linux-headers,
	# simlar to what ubuntu does
	#
	# this way you dont need to install the 300-400 kernel sources to
	# build a tiny kernel module
	#
	pkgdesc="Headers and script for third party modules for $_flavor kernel"
	depends="$_depends_dev"
	local dir="$subpkgdir"/usr/src/linux-headers-${_abi_release}
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	# first we import config, run prepare to set up for building
	# external modules, and create the scripts
	mkdir -p "$dir"
	cp "$srcdir"/config-$_flavor.${CARCH} "$dir"/.config
	echo "-$pkgrel-$_flavor" > "$dir"/localversion-alpine
	cd $builddir

	echo "Installing headers..."
	case "$_carch" in
	x86_64)
		_carch="x86"
		install -Dt "${dir}/tools/objtool" $builddir/tools/objtool/objtool
		;;
	esac
	cp -t "$dir" -a $builddir/include

	install -Dt "${dir}" -m644 $builddir/Makefile
	install -Dt "${dir}" -m644 $builddir/Module.symvers
	install -Dt "${dir}" -m644 $builddir/System.map
	cp -t "$dir" -a $builddir/scripts

	install -Dt "${dir}/arch/${_carch}" -m644 $builddir/arch/${_carch}/Makefile
	install -Dt "${dir}/arch/${_carch}/kernel" -m644 $builddir/arch/${_carch}/kernel/asm-offsets.s
	cp -t "${dir}/arch/${_carch}" -a $builddir/arch/${_carch}/include

	install -Dt "$dir/drivers/md" -m644 drivers/md/*.h
	install -Dt "$dir/net/mac80211" -m644 net/mac80211/*.h

	# https://bugs.archlinux.org/task/13146
	install -Dt "$dir/drivers/media/i2c" -m644 drivers/media/i2c/msp3400-driver.h

	# https://bugs.archlinux.org/task/20402
	install -Dt "$dir/drivers/media/usb/dvb-usb" -m644 drivers/media/usb/dvb-usb/*.h
	install -Dt "$dir/drivers/media/dvb-frontends" -m644 drivers/media/dvb-frontends/*.h
	install -Dt "$dir/drivers/media/tuners" -m644 drivers/media/tuners/*.h

	# https://bugs.archlinux.org/task/71392
	install -Dt "$dir/drivers/iio/common/hid-sensors" -m644 drivers/iio/common/hid-sensors/*.h

	echo "Installing KConfig files..."
	find . -name 'Kconfig*' -exec install -Dm644 {} "$dir/{}" \;

	echo "Removing unneeded architectures..."
	local arch
	for arch in "$dir"/arch/*/; do
		case $(basename "$arch") in $_carch) continue ;; esac
		echo "Removing $(basename "$arch")"
		rm -r "$arch"
	done

	echo "Removing broken symlinks..."
	find -L "$builddir" -type l -printf 'Removing %P\n' -delete

	echo "Removing loose objects..."
	find "$builddir" -type f -name '*.o' -printf 'Removing %P\n' -delete

	echo "Stripping build tools..."
	local file
	while read -rd '' file; do
		case "$(file -bi "$file")" in
			application/x-sharedlib\;*)      # Libraries (.so)
				strip -v $STRIP_SHARED "$file" ;;
			application/x-archive\;*)        # Libraries (.a)
				strip -v $STRIP_STATIC "$file" ;;
			application/x-executable\;*)     # Binaries
				strip -v $STRIP_BINARIES "$file" ;;
			application/x-pie-executable\;*) # Relocatable binaries
				strip -v $STRIP_SHARED "$file" ;;
		esac
	done < <(find "$builddir" -type f -perm -u+x ! -name vmlinux -print0)

	echo "Stripping vmlinux..."
	strip -v $STRIP_STATIC "$builddir/vmlinux"

	echo "Adding symlink..."
	mkdir -p "$subpkgdir"/lib/modules/${_abi_release}
	ln -sf /usr/src/linux-headers-${_abi_release} \
		"$subpkgdir"/lib/modules/${_abi_release}/build
}

_doc() {
	pkgdesc="documentation for $_flavor kernel"
	mkdir -p "$subpkgdir"/usr/share/doc/linux-edge-doc
	cp -r "$builddir"/Documentation \
		"$subpkgdir"/usr/share/doc/linux-edge-doc/

}

sha512sums="
baa2487954044f991d2ae254d77d14a1f0185dd62c9f0fcaff69f586c9f906823017b8db1c4588f27b076dfa3ebb606929fec859f60ea419e7974330b9289cc2  linux-6.10.tar.xz
ed9290c0f0c25d2e0bf1c273e51cac1017b9218e545c7fd7f9074456a1e7a0c2b6231a5ba5ab1538557fe3422f6cf9f7bd2ab368c83bf5c34550eacbaa995d55  patch-6.10.7.xz
bc15fb1257029dbd00700974724574b9412b28354771bb0ebda452ce29a8eca60026c7d8bbbaf642cc01a64df0b607f7b11519d64ddb62ff31e0d31b287cbeb1  config-edge.aarch64
53786e34d7ed16fe5bb29dbc2ed89b3188a553358f9a6153216d13f5f66fb5c5a623c8be2f090e14dcabf5a9b19c19d865c643207978a9e4f1ce9551251fa576  config-edge.armv7
849a060e79c9f2d0deb0141ef68a3680f231f37b1d47e4c62fa2122a2a236132910c02d250d8ff30a4c7f6b6527a51683dc9daf23281363e2564a0ea24c62e98  config-edge.x86_64
41f117bb29d1544bc966f6474b04a600fdda81cc9b19636fdbe3392de4d89ba7a785aa5e548b8333eac9451b99277444b9dee96db45eb9a58817fff94504320c  config-edge.riscv64
"
