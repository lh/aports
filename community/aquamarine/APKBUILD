# Contributor: Zach DeCook <zachdecook@librem.one>
# Maintainer: Zach DeCook <zachdecook@librem.one>
pkgname=aquamarine
pkgver=0.3.2
pkgrel=0
pkgdesc="Aquamarine is a very light linux rendering backend library"
url="https://github.com/hyprwm/aquamarine"
arch="all"
license="BSD-3-Clause"
source="
	https://github.com/hyprwm/aquamarine/archive/v$pkgver/aquamarine-v$pkgver.tar.gz
	use-legacy-libgl.patch
	"
makedepends="
	cmake
	eudev-dev
	hwdata-dev
	hyprutils-dev>=0.1.5
	hyprwayland-scanner>=0.4.0
	libdisplay-info-dev
	libinput-dev
	libseat-dev
	mesa-dev
	pixman-dev
	wayland-dev
	wayland-protocols
	"
subpackages="$pkgname-doc $pkgname-dev"
options="!check" # tests are broken

build() {
	cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=None
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	install -Dm644 LICENSE "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}


sha512sums="
ed5e17cbbf6bff579db657366e7035b765979821fc2be95c420b290f1f25625d29c25ac30ab49183b58654ae442f18382f8bb0931aae57a796a24a14f4baacea  aquamarine-v0.3.2.tar.gz
ed439fbece3a5843a644487e349bcd27dc1ed3494e1237bb716774e94d16a9e20850f2eaeec930e4cd5a5280545932b68b18da1c801d08cf13abd62418cc09b5  use-legacy-libgl.patch
"
