# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=mimetreeparser
pkgver=24.08.0
pkgrel=0
pkgdesc="Parser for MIME trees"
# armhf blocked by extra-cmake-modules
# s390x: blocked by kmbox
arch="all !armhf !s390x"
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND LGPL-2.0-only AND LGPL-2.1-or-later AND BSD-3-Clause AND GPL-2.0-or-later"
makedepends="
	doxygen
	extra-cmake-modules
	gpgme-dev
	graphviz
	kcalendarcore-dev
	kcodecs-dev
	ki18n-dev
	kmbox-dev
	kmime-dev
	kwidgetsaddons-dev
	libkleo-dev
	qt6-qt5compat-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	qt6-qttools-dev
	samurai
	"
checkdepends="
	gpg-agent
	xvfb-run
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/pim/mimetreeparser.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/mimetreeparser-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure -E "(core-attachment|core-cryptohelper|core-mimetreeparser|core-gpgerror|core-partmodel|widgets-messageviewer|widgets-messageviewerdialog)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
aa37ba8a202e1a9d11fe4dbfda7365af4fa63dc26d304f743dbeb7535fa37401f4a21a2ee36d1cf8d40acbf9753536f4fd167c2163c52d9b62423290f96ec041  mimetreeparser-24.08.0.tar.xz
"
