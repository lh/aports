# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libkdcraw
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org"
pkgdesc="RAW image file format support for KDE"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
depends_dev="
	libraw-dev
	qt6-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
subpackages="$pkgname-dev"
_repo_url="https://invent.kde.org/graphics/libkdcraw.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkdcraw-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f144011e6ca6402d1078bc1cb9ba8ad8adf608a98b5d21201a8e18d9fa7c8bfbf04911755aca8bf4dabe056711d99df15d52acc8a4e3d15b1b8d55b4db65f97a  libkdcraw-24.08.0.tar.xz
"
