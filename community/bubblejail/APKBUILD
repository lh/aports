# Maintainer: Donoban <donoban@riseup.net>
pkgname=bubblejail
pkgver=0.9.1
pkgrel=0
pkgdesc="Bubblewrap based sandboxing for desktop applications"
url="https://github.com/igo95862/bubblejail"
arch="noarch"
license="GPL-3.0-or-later"
depends="
	bubblewrap
	libseccomp
	py3-lxns
	py3-tomli-w
	py3-xdg
	xdg-dbus-proxy
	"
makedepends="
	meson
	scdoc
	py3-jinja2
	"
subpackages="
	$pkgname-doc
	$pkgname-pyc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-config
	"
source="bubblejail-$pkgver.tar.gz::https://github.com/igo95862/bubblejail/archive/refs/tags/$pkgver.tar.gz
				site-packages.patch"
options="!check" # No tests

build() {
	abuild-meson \
		-Dversion_display="Alpine $pkgver" \
		. output
	meson compile -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

config() {
	depends="$pkgname=$pkgver-r$pkgrel py3-qt6"
	amove /usr/bin/bubblejail-config
	amove /usr/lib/python3.*/site-packages/bubblejail/bubblejail_gui_qt.py
}

sha512sums="
9bdb5b54e48698e718a531affd0058bab22715fe1d85b06300d509b0b844d4c5bdfaa5d14a9bffaecd4c7cebb1d9c244d5d303a6cede44f650093bd502c99698  bubblejail-0.9.1.tar.gz
6a605729fc0c2d722beaa40f7c71c50ea7dd431bac48dc8d48ac6e597d5a9750b2b516973040e76f98d804c983851455d885d37123d178b3f01eafc53d48a0fd  site-packages.patch
"
